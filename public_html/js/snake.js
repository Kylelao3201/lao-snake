/*--------------------------------------------------------------------------------------------------------------------------
 *Varibles 
 *-------------------------------------------------------------------------------------------------------------------------- 
 */

var snake;
var snakeLength;
var snakeSize;
var snakeDirection;

var food;

var context;
var screenWidth;
var screenHeight;

var gameState;
var gameOverMenu;
var startMenu;

var restartButton;
var backButton;
var easyButton;
var mediumButton;
var hardButton;

var playHUD;
var scoreBoard;

var crashSound;
var blopSound;
var dubstepSound;

var image;

var myInterval;
/*--------------------------------------------------------------------------------------------------------------------------
 * Exucuting Game Code 
 * -------------------------------------------------------------------------------------------------------------------------
 */

gameInitialize();
snakeInitialize();
foodInitialize();
myInterval = setInterval(gameLoop, 1000 / 5);

/*--------------------------------------------------------------------------------------------------------------------------
 * Game Function
 * -------------------------------------------------------------------------------------------------------------------------
 */

function gameInitialize()
{
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");

    screenWidth = window.innerWidth - 30;
    screenHeight = window.innerHeight - 30;

    canvas.width = screenWidth;
    canvas.height = screenHeight;

    document.addEventListener("keydown", keyboardHandler);

    gameOverMenu = document.getElementById("gameOver");
    centerMenuPosition(gameOverMenu);

    restartButton = document.getElementById("restartButton");
    restartButton.addEventListener("click", gameRestart);

    backButton = document.getElementById("backButton");
    backButton.addEventListener("click", back);

    startMenu = document.getElementById("startMenu");
    centerMenuPosition(startMenu);

    easyButton = document.getElementById("easyButton");
    easyButton.addEventListener("click", easy);

    mediumButton = document.getElementById("mediumButton");
    mediumButton.addEventListener("click", medium);

    hardButton = document.getElementById("hardButton");
    hardButton.addEventListener("click", hard);

    playHUD = document.getElementById("playHUD");
    scoreBoard = document.getElementById("scoreboard");
    crashSound = new Audio("sounds/crash.mp3");
    crashSound.preload = "auto";
    blopSound = new Audio("sounds/blop.mp3");
    blopSound.preload = "auto";
    dupstepSound = new Audio("sounds/dupstep.mp3");
    dupstepSound.preload = "auto";

    image = document.getElementById("source");

    setState("START MENU");
}

function gameLoop()
{
    gameDraw();
    drawScoreBoard();
    if (gameState === "PLAY")
    {
        snakeUpdate();
        snakeDraw();
        foodDraw();

    }
}

function gameDraw()
{
    context.fillStyle = "rgb(36, 164, 255)";
    context.fillRect(0, 0, screenWidth, screenHeight);
}

function easy()
{
    snakeInitialize();
    foodInitialize();
    clearInterval(myInterval);
    myInterval = setInterval(gameLoop, 1000 / 15);
    hideMenu(startMenu);
    setState("PLAY");
    dupstepSound.play();
}

function medium()
{
    snakeInitialize();
    foodInitialize();
    clearInterval(myInterval);
    myInterval = setInterval(gameLoop, 1000 / 25);
    hideMenu(startMenu);
    setState("PLAY");
    dupstepSound.play();
}

function hard()
{
    snakeInitialize();
    foodInitialize();
    clearInterval(myInterval);
    myInterval = setInterval(gameLoop, 1000 / 40);
    hideMenu(startMenu);
    setState("PLAY");
    dupstepSound.play();
}

function gameRestart()
{
    snakeInitialize();
    foodInitialize();
    hideMenu(gameOverMenu);
    setState("PLAY");
    dupstepSound.play();
}

function back()
{
    snakeInitialize();
    foodInitialize();
    hideMenu(gameOverMenu);
    setState("START MENU");
}
/*--------------------------------------------------------------------------------------------------------------------------
 * Snake Function 
 * -------------------------------------------------------------------------------------------------------------------------
 */

function snakeInitialize()
{
    snake = [];
    snakeLength = 1;
    snakeSize = 20;
    snakeDirection = "down";

    for (var index = snakeLength - 1; index >= 0; index--)
    {
        snake.push
                ({
                    x: index,
                    y: 0
                });
    }
}

function snakeDraw()
{
    for (var index = 0; index < snake.length; index++)
    {
        var remainder = index % 2;
        if (remainder === 0)
        {
            context.fillStyle = "white";
        }
        else
        {
            context.fillStyle = "#FF7700";
            
        }
        context.strokeStyle = "black";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
    }
}

function snakeUpdate()
{
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;

    if (snakeDirection === "down")
    {
        snakeHeadY++;
    }
    else if (snakeDirection === "right")
    {
        snakeHeadX++;
    }
    else if (snakeDirection === "left")
    {
        snakeHeadX--;
    }
    else if (snakeDirection === "up")
    {
        snakeHeadY--;
    }

    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);

    var snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
}

/*-------------------------------------------------------------------------------------------------------------------------
 * Food Function
 * ------------------------------------------------------------------------------------------------------------------------
 */

function foodInitialize()
{
    food = {
        x: 0,
        y: 0
    };
    setFoodPosition();
}

function foodDraw()
{
    context.drawImage(image, food.x * snakeSize, food.y * snakeSize, snakeSize, snakeSize);
}

function setFoodPosition()
{
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);

    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}
/*------------------------------------------------------------------------------------------------------------------------
 * Input Functions
 * -----------------------------------------------------------------------------------------------------------------------
 */
function keyboardHandler(event)
{
    console.log(event);

    if (event.keyCode === 39 && snakeDirection !== "left") {
        snakeDirection = "right";
    }

    else if (event.keyCode === 40 && snakeDirection !== "up") {
        snakeDirection = "down";
    }

    else if (event.keyCode === 38 && snakeDirection !== "down") {
        snakeDirection = "up";
    }

    else if (event.keyCode === 37 && snakeDirection !== "right") {
        snakeDirection = "left";
    }
}

function checkFoodCollisions(snakeHeadX, snakeHeadY)
{
    if (snakeHeadX === food.x && snakeHeadY === food.y)
    {
        snake.push
                ({
                    x: 0,
                    y: 0
                });
        snakeLength++;
        setFoodPosition();
        while (snakeHeadX === food.x && snakeHeadY === food.y)
        {
            setFoodPosition();
        }
        blopSound.play();
    }
}

function checkWallCollisions(snakeHeadX, snakeHeadY)
{
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0)
    {
        crashSound.play();
        setState("GAME OVER");
        dupstepSound.pause();
        dupstepSound.currentTime = 0;
        
    }
    else if (snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0)
    {
        crashSound.play();
        setState("GAME OVER");
        dupstepSound.pause();
        dupstepSound.currentTime = 0;
        
    }
}

function checkSnakeCollisions(snakeHeadX, snakeHeadY)
{
    for (var index = 1; index < snake.length; index++)
    {
        if (snakeHeadX === snake[index].x && snakeHeadY === snake[index].y)
        {
            crashSound.play();            
            setState("GAME OVER");
            dupstepSound.pause();
            dupstepSound.currentTime = 0;
            return;
        }
    }
}
/*---------------------------------------------------------------------------------------------------------------------------
 * Game STATE
 ----------------------------------------------------------------------------------------------------------------------------
 */

function setState(state)
{
    gameState = state;
    showMenu(state);
}


/*--------------------------------------------------------------------------------------------------------------------------
 * Menu Fuction
 * -------------------------------------------------------------------------------------------------------------------------
 */
function displayMenu(menu)
{
    menu.style.visibility = "visible";
}

function hideMenu(menu)
{
    menu.style.visibility = "hidden";
}

function showMenu(state)
{
    if (state === "GAME OVER")
    {
        displayMenu(gameOverMenu);
    }
    else if (state === "PLAY")
    {
        displayMenu(playHUD);
    }
    else if (state === "START MENU")
    {
        displayMenu(startMenu);
    }
}

function centerMenuPosition(menu)
{
    menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
    menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
}

function drawScoreBoard()
{
    scoreBoard.innerHTML = "Score: " + snakeLength;
}